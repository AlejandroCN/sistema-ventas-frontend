import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CarritoComponent } from './components/shared/carrito/carrito.component';
import { VentaComponent } from './components/venta/venta.component';
import { PagosComponent } from './components/pagos/pagos.component';
import { VentasClienteComponent } from './components/ventas-cliente/ventas-cliente.component';
import { VentasFacturaComponent } from './components/reportes/ventas-factura/ventas-factura.component';
import { VentaArticulosComponent } from './components/reportes/venta-articulos/venta-articulos.component';
import { VentasPorClienteComponent } from './components/reportes/ventas-por-cliente/ventas-por-cliente.component';
import { VentasMesComponent } from './components/reportes/ventas-mes/ventas-mes.component';
import { VentasMesClienteComponent } from './components/reportes/ventas-mes-cliente/ventas-mes-cliente.component';
import { ExistenciasComponent } from './components/reportes/existencias/existencias.component';
import { HistoricoComponent } from './components/reportes/historico/historico.component';
import { PagosReporteComponent } from './components/reportes/pagos-reporte/pagos-reporte.component';
import { PagosDiaComponent } from './components/reportes/pagos-dia/pagos-dia.component';

export const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'carrito', component: CarritoComponent },
    { path: 'venta', component: VentaComponent },
    { path: 'pagos', component: PagosComponent },
    { path: 'ventasCliente/:idCliente', component: VentasClienteComponent },
    { path: 'ventasFactura', component: VentasFacturaComponent },
    { path: 'ventasArticulos', component: VentaArticulosComponent },
    { path: 'ventasPorCliente', component: VentasPorClienteComponent },
    { path: 'ventasMes', component: VentasMesComponent },
    { path: 'ventasMesCliente', component: VentasMesClienteComponent },
    { path: 'existencias', component: ExistenciasComponent },
    { path: 'historico', component: HistoricoComponent },
    { path: 'pagosReporte', component: PagosReporteComponent },
    { path: 'pagosDia', component: PagosDiaComponent },

    // Es una ruta cualquiera que va a redireccionar a home
    { path: '', pathMatch: 'full', redirectTo: 'home' },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];
