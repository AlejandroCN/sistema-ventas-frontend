import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';

// Importacion de las rutas
import { ROUTES } from './app.routes';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { CategoriasService } from './services/categorias.service';
import { ArticulosService } from './services/articulos.service';
import { ClientesService } from './services/clientes.service';
import { FormasPagoService } from './services/formas-pago.service';
import { VentaService } from './services/venta.service';
import { CarritoComponent } from './components/shared/carrito/carrito.component';
import { VentaComponent } from './components/venta/venta.component';
import { PagosComponent } from './components/pagos/pagos.component';
import { VentasClienteComponent } from './components/ventas-cliente/ventas-cliente.component';
import { VentasFacturaComponent } from './components/reportes/ventas-factura/ventas-factura.component';
import { VentaArticulosComponent } from './components/reportes/venta-articulos/venta-articulos.component';
import { VentasPorClienteComponent } from './components/reportes/ventas-por-cliente/ventas-por-cliente.component';
import { VentasMesComponent } from './components/reportes/ventas-mes/ventas-mes.component';
import { VentasMesClienteComponent } from './components/reportes/ventas-mes-cliente/ventas-mes-cliente.component';
import { ExistenciasComponent } from './components/reportes/existencias/existencias.component';
import { HistoricoComponent } from './components/reportes/historico/historico.component';
import { PagosReporteComponent } from './components/reportes/pagos-reporte/pagos-reporte.component';
import { PagosDiaComponent } from './components/reportes/pagos-dia/pagos-dia.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    CarritoComponent,
    VentaComponent,
    PagosComponent,
    VentasFacturaComponent,
    VentaArticulosComponent,
    VentasClienteComponent,
    VentasPorClienteComponent,
    VentasMesComponent,
    VentasMesClienteComponent,
    ExistenciasComponent,
    HistoricoComponent,
    PagosReporteComponent,
    PagosDiaComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot( ROUTES )
  ],
  providers: [
    CategoriasService,
    ArticulosService,
    ClientesService,
    FormasPagoService,
    VentaService,
    CarritoComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
