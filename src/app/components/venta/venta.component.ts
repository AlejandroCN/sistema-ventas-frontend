import { Component } from '@angular/core';
import { Carrito } from 'src/app/models/Carrito';
import { ClientesService } from 'src/app/services/clientes.service';
import { VendedoresService } from 'src/app/services/vendedores.service';
import { FormasPagoService } from 'src/app/services/formas-pago.service';
import { DireccionesClienteService } from 'src/app/services/direcciones-cliente.service';
import { VentaService } from 'src/app/services/venta.service';
import { Router } from '@angular/router';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.css']
})
export class VentaComponent {

  // Almacena el total a pagar del cliente
  total: number;

  // Almacena todos los clientes que existan en la base de datos para mostrarlos
  clientes: any[] = [];

  // Todos los vendedores que existan en la base de datos
  vendedores: any[] = [];

  // Todas las formas de pago registradas
  formasPago: any[] = [];

  // Todas las direcciones que tenga un cliente seleccionado
  direccionesCliente: any[] = [];

  constructor(public carrito: Carrito,
              private clientesService: ClientesService,
              private vendedoresService: VendedoresService,
              private formasPagoService: FormasPagoService,
              private direccionesClienteService: DireccionesClienteService,
              private ventaService: VentaService,
              private router: Router) {
    this.total = 0;

    for (const articulo of carrito.articulos) {
      this.total += articulo.cantidad * articulo.precio;
    }

    this.clientesService.getClientes().subscribe((data: any) => {
      this.clientes = data;
    });

    this.vendedoresService.getVendedores().subscribe((data: any) => {
      this.vendedores = data;
    });

    this.formasPagoService.getFormasPago().subscribe((data: any) => {
      this.formasPago = data;
    });
  }

  obtenerDireccionesCliente(idCliente: number) {
    this.direccionesClienteService.getDireccionesCliente(idCliente).subscribe((data: any) => {
      this.direccionesCliente = data;
    });
  }

  nuevaDireccionCliente(direccion: string, idCliente: number) {
    this.direccionesClienteService.saveDireccionCliente(direccion, idCliente).subscribe((data: any) => {
      console.log(data);
      this.obtenerDireccionesCliente(idCliente);
    });
    console.log("Alta de nueva direccion");
    console.log(this.direccionesCliente);
  }

  registrarCompra(idC: number, idVend: number, idFP: number, idDirec: number, pago: number) {
    const ventaCompleta = {
      articulos: this.carrito.articulos,
      venta: {
        id: null,
        idCliente: idC,
        idVendedor: idVend,
        idFormaPago: idFP,
        fecha: this.obtenerFecha(),
        total: 0,
        saldo: 0
      },
      total: this.total,
      pagado: pago,
      idDireccion: idDirec
    };

    this.ventaService.saveVenta(ventaCompleta).subscribe((data: any) => {
      alert('Compra realizada con exito, haga clic en aceptar para descargar su detalle en pdf');
      this.generarPdf(ventaCompleta.articulos);
      this.vaciarCarrito();
      this.router.navigate(['/home']);
    });
  }

  vaciarCarrito() {
    this.carrito.articulos = [];
  }

  // Devuelve una fecha de la forma YYYY-MM-DD
  obtenerFecha() {
    const fechaActual = new Date();
    const anio = fechaActual.getFullYear();
    const mes = fechaActual.getMonth() + 1;
    const dia = fechaActual.getDate();
    let fechaFormateada: any;

    if (mes < 10) {
      fechaFormateada = anio + '-0' + mes + '-' + dia;
    } else {
      fechaFormateada = anio + '-' + mes + '-' + dia;
    }
    return fechaFormateada;
  }

  generarPdf(articulos: any) {
    const pdf = new jsPDF();
    pdf.text(20, 20, 'Detalle de la compra');

    const columns = ['Nombre Producto', 'Precio', 'Cantidad'];
    const rows = this.jsonToArray(articulos);

    pdf.autoTable(columns, rows,
        { margin: { top: 25 } }
    );

    pdf.save('venta.pdf');
  }

  jsonToArray(json: any) {
    const articulos: any[] = [];
    for ( const art of json ) {
      const elem: any[] = [];
      elem.push(art.nombre);
      elem.push(art.precio);
      elem.push(art.cantidad);

      articulos.push(elem);
    }
    const rowTotal: any[] = ['', 'TOTAL:', `$${this.total}.00`];
    articulos.push(rowTotal);

    return articulos;
  }

}
