import { Component } from '@angular/core';
import { ClientesService } from '../../services/clientes.service';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.css']
})
export class PagosComponent {

  // Almacena los clientes que sean buscados en la base de datos
  clientes: any[] = [];

  // Es el cliente que se seleccione de la lista
  clienteSeleccionado: any = {};

  constructor( private clienteService: ClientesService ) {
    this.buscarTodosLosClientes();
  }

  buscarTodosLosClientes() {
    this.clienteService.getClientes().subscribe( (data: any) => {
      this.clientes = data;
    });
  }

  buscarClientesPorTermino(termino: string) {
    this.clienteService.getClientesByTermino(termino).subscribe( (data: any) => {
      this.clientes = data;
    });
  }

}
