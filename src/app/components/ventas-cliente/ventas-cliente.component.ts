import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VentaService } from '../../services/venta.service';
import { ClientesService } from '../../services/clientes.service';
import { PagosService } from '../../services/pagos.service';

@Component({
  selector: 'app-ventas-cliente',
  templateUrl: './ventas-cliente.component.html',
  styleUrls: ['./ventas-cliente.component.css']
})
export class VentasClienteComponent {

  // Almacena todas las ventas que tengan saldo deudor de un determinado cliente
  ventas: any[] = [];

  // El id del cliente que se recibe por parametro
  idCliente: number;

  // Es el cliente que se busca segun el id recibido
  cliente: any = {};

  // Es la venta seleccionada para hacerle un pago
  ventaSeleccionada: any = {};

  constructor( private activatedRoute: ActivatedRoute,
               private ventasService: VentaService,
               private clientesService: ClientesService,
               private pagosService: PagosService ) {
    this.idCliente = this.activatedRoute.snapshot.params.idCliente;
    this.buscarClientePorId();
    this.buscarVentasDeudoras();
  }

  buscarClientePorId() {
    this.clientesService.getClienteById(this.idCliente).subscribe( (data: any) => {
      this.cliente = data;
    });
  }

  buscarVentasDeudoras() {
    this.ventasService.getVentasDeudorasByCliente(this.idCliente).subscribe( (data: any) => {
      this.ventas = data;
    });
  }

  realizarPago(monto: string) {
    const pagoParcial = {
      id: null,
      idVenta: this.ventaSeleccionada.id,
      pago: monto,
      fecha: this.obtenerFecha()
    };

    this.pagosService.savePago(pagoParcial).subscribe( (data: any) => {
      alert('Se ha realizado su pago correctamente');
      window.location.reload();
    });
  }

  // Devuelve una fecha de la forma YYYY-MM-DD
  obtenerFecha() {
    const fechaActual = new Date();
    const anio = fechaActual.getFullYear();
    const mes = fechaActual.getMonth() + 1;
    const dia = fechaActual.getDate();
    let fechaFormateada: any;

    if (mes < 10) {
      fechaFormateada = anio + '-0' + mes + '-' + dia;
    } else {
      fechaFormateada = anio + '-' + mes + '-' + dia;
    }
    return fechaFormateada;
  }

}
