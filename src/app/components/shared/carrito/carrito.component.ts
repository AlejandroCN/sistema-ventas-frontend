import { Component, OnInit, Injectable } from '@angular/core';
import { Carrito } from '../../../models/Carrito';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {

  suma: number;

  constructor( public carrito: Carrito ) {
    this.mostrarCarrito();
    this.suma = 0;
  }

  ngOnInit() {
    for ( const articulo of this.carrito.articulos ) {
      this.suma += articulo.cantidad * articulo.precio;
    }
  }

  mostrarCarrito() {
    console.log(this.carrito.articulos);
  }

}

/*export interface ArticuloEnCarrito {
  id: number;
  nombre: string;
  precio: number;
  cantidad: number;
}*/
