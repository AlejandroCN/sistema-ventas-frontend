import { Component } from '@angular/core';
import { ReportesService } from '../../../services/reportes.service';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-venta-articulos',
  templateUrl: './venta-articulos.component.html',
  styleUrls: ['./venta-articulos.component.css']
})
export class VentaArticulosComponent {

  ventasPorArticulos: any[] = [];

  constructor( private reportesService: ReportesService ) {
    this.getVentasPorArticulos();
  }

  getVentasPorArticulos() {
    this.reportesService.getVentasPorArticulo().subscribe( (data: any) => {
      this.ventasPorArticulos = data;
    });
  }

  generarPDF() {
    const pdf = new jsPDF();
    pdf.text(20, 20, 'Ventas por Articulos');

    const columns = ['Id Articulo', 'Nombre Articulo', 'Cantidad Vendida', 'Total'];
    const rows = this.jsonToArray(this.ventasPorArticulos);

    pdf.autoTable(columns, rows,
        { margin: { top: 25 } }
    );

    pdf.save('ventasPorArticulos.pdf');
  }

  jsonToArray(json: any) {
    const arreglo: any[] = [];
    for ( const venta of json ) {
      const row: any[] = [];
      row.push(venta.idArticulo);
      row.push(venta.nombreArticulo);
      row.push(venta.cantidadVendida);
      row.push(venta.total);

      arreglo.push(row);
    }
    return arreglo;
  }

}
