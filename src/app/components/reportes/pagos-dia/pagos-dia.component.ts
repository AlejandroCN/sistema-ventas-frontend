import { Component } from '@angular/core';
import { ReportesService } from '../../../services/reportes.service';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-pagos-dia',
  templateUrl: './pagos-dia.component.html',
  styleUrls: ['./pagos-dia.component.css']
})
export class PagosDiaComponent {

  pagos: any[] = [];

  constructor( private reportesService: ReportesService ) {
    this.getPagosByDia();
  }

  getPagosByDia() {
    this.reportesService.getPagosPorDia().subscribe( (data: any) => {
      this.pagos = data;
    });
  }

  generarPDF() {
    const pdf = new jsPDF();
    pdf.text(20, 20, 'Pagos recibidos por dia');

    const columns = ['Dia', 'Total Pagos'];
    const rows = this.jsonToArray(this.pagos);

    pdf.autoTable(columns, rows,
        { margin: { top: 25 } }
    );

    pdf.save('pagosPorDia.pdf');
  }

  jsonToArray(json: any) {
    const arreglo: any[] = [];
    for ( const pago of json ) {
      const row: any[] = [];
      row.push(pago.dia);
      row.push(pago.totalPagos);

      arreglo.push(row);
    }
    return arreglo;
  }

}
