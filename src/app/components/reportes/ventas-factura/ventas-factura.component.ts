import { Component } from '@angular/core';
import { ReportesService } from '../../../services/reportes.service';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-ventas-factura',
  templateUrl: './ventas-factura.component.html',
  styleUrls: ['./ventas-factura.component.css']
})
export class VentasFacturaComponent {

  ventasPorFactura: any[] = [];

  constructor( private reportesService: ReportesService ) {
    this.obtenerVentasPorFactura();
  }

  obtenerVentasPorFactura() {
    this.reportesService.getVentasPorFactura().subscribe( (data: any) => {
      this.ventasPorFactura = data;
    });
  }

  generarPDF() {
    const pdf = new jsPDF();
    pdf.text(20, 20, 'Ventas por factura');

    const columns = ['Cliente', 'Folio Venta', 'Fecha', 'Total'];
    const rows = this.jsonToArray(this.ventasPorFactura);

    pdf.autoTable(columns, rows,
        { margin: { top: 25 } }
    );

    pdf.save('ventasPorFactura.pdf');
  }

  jsonToArray(json: any) {
    const arreglo: any[] = [];
    for ( const venta of json ) {
      const row: any[] = [];
      row.push(venta.nombreCliente);
      row.push(venta.folioVenta);
      row.push(venta.fecha);
      row.push(venta.total);

      arreglo.push(row);
    }
    return arreglo;
  }

}
