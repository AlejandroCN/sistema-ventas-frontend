import { Component } from '@angular/core';
import { ReportesService } from '../../../services/reportes.service';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-historico',
  templateUrl: './historico.component.html',
  styleUrls: ['./historico.component.css']
})
export class HistoricoComponent {

  historico: any[] = [];

  constructor( private reportesService: ReportesService ) {
    this.getHistoricoVentas();
  }

  getHistoricoVentas() {
    this.reportesService.getHistoricoVentas().subscribe( (data: any) => {
      this.historico = data;
    });
  }

  generarPDF() {
    const pdf = new jsPDF();
    pdf.text(20, 20, 'Historico de Ventas');

    const columns = ['Nombre Cliente', 'Clave de Venta', 'Fecha', 'Importe Total', 'Saldo Deudor'];
    const rows = this.jsonToArray(this.historico);

    pdf.autoTable(columns, rows,
        { margin: { top: 25 } }
    );

    pdf.save('historicoVentas.pdf');
  }

  jsonToArray(json: any) {
    const arreglo: any[] = [];
    for ( const venta of json ) {
      const row: any[] = [];
      row.push(venta.nombreCliente);
      row.push(venta.idVenta);
      row.push(venta.fecha);
      row.push(venta.total);
      row.push(venta.saldo);

      arreglo.push(row);
    }
    return arreglo;
  }

}
