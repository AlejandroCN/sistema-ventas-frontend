import { Component } from '@angular/core';
import { ReportesService } from '../../../services/reportes.service';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-existencias',
  templateUrl: './existencias.component.html',
  styleUrls: ['./existencias.component.css']
})
export class ExistenciasComponent {

  existencias: any[] = [];

  constructor( private reportesService: ReportesService ) {
    this.getExistencias();
  }

  getExistencias() {
    this.reportesService.getExistencias().subscribe( (data: any) => {
      this.existencias = data;
    });
  }

  generarPDF() {
    const pdf = new jsPDF();
    pdf.text(20, 20, 'Existencias de articulos');

    const columns = ['Id Articulo', 'Nombre Articulo', 'Existencias'];
    const rows = this.jsonToArray(this.existencias);

    pdf.autoTable(columns, rows,
        { margin: { top: 25 } }
    );

    pdf.save('existencias.pdf');
  }

  jsonToArray(json: any) {
    const arreglo: any[] = [];
    for ( const venta of json ) {
      const row: any[] = [];
      row.push(venta.id);
      row.push(venta.nombreArticulo);
      row.push(venta.stock);

      arreglo.push(row);
    }
    return arreglo;
  }

}
