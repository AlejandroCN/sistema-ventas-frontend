import { Component } from '@angular/core';
import { ReportesService } from '../../../services/reportes.service';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-ventas-mes',
  templateUrl: './ventas-mes.component.html',
  styleUrls: ['./ventas-mes.component.css']
})
export class VentasMesComponent {

  ventasPorMes: any[] = [];

  constructor( private reporteService: ReportesService ) {
    this.getVentasPorMes();
  }

  getVentasPorMes() {
    this.reporteService.getVentasPorMes().subscribe( (data: any) => {
      this.ventasPorMes = data;
    });
  }

  generarPDF() {
    const pdf = new jsPDF();
    pdf.text(20, 20, 'Ventas por Mes');

    const columns = ['Mes de Venta', 'Total Vendido'];
    const rows = this.jsonToArray(this.ventasPorMes);

    pdf.autoTable(columns, rows,
        { margin: { top: 25 } }
    );

    pdf.save('ventasPorMes.pdf');
  }

  jsonToArray(json: any) {
    const arreglo: any[] = [];
    for ( const venta of json ) {
      const row: any[] = [];
      row.push(venta.mes);
      row.push(venta.totalVendido);

      arreglo.push(row);
    }
    return arreglo;
  }

}
