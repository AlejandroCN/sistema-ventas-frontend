import { Component } from '@angular/core';
import { ReportesService } from '../../../services/reportes.service';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-ventas-por-cliente',
  templateUrl: './ventas-por-cliente.component.html',
  styleUrls: ['./ventas-por-cliente.component.css']
})
export class VentasPorClienteComponent {

  ventasPorCliente: any[] = [];

  constructor( private reportesService: ReportesService ) {
    this.getVentasPorCliente();
  }

  getVentasPorCliente() {
    this.reportesService.getVentasPorCliente().subscribe( (data: any) => {
      this.ventasPorCliente = data;
    });
  }

  generarPDF() {
    const pdf = new jsPDF();
    pdf.text(20, 20, 'Ventas por Cliente');

    const columns = ['Id Cliente', 'Nombre Cliente', 'Total Vendido'];
    const rows = this.jsonToArray(this.ventasPorCliente);

    pdf.autoTable(columns, rows,
        { margin: { top: 25 } }
    );

    pdf.save('ventasPorCliente.pdf');
  }

  jsonToArray(json: any) {
    const arreglo: any[] = [];
    for ( const venta of json ) {
      const row: any[] = [];
      row.push(venta.idCliente);
      row.push(venta.nombreCliente);
      row.push(venta.totalVendido);

      arreglo.push(row);
    }
    return arreglo;
  }

}
