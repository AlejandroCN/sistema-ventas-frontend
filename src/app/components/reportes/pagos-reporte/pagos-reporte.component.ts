import { Component } from '@angular/core';
import { ReportesService } from '../../../services/reportes.service';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-pagos-reporte',
  templateUrl: './pagos-reporte.component.html',
  styleUrls: ['./pagos-reporte.component.css']
})
export class PagosReporteComponent {

  pagos: any[] = [];

  constructor( private reportesService: ReportesService ) {
    this.getPagosByMes();
  }

  getPagosByMes() {
    this.reportesService.getPagosPorMes().subscribe( (data: any) => {
      this.pagos = data;
    });
  }

  generarPDF() {
    const pdf = new jsPDF();
    pdf.text(20, 20, 'Pagos recibidos por mes');

    const columns = ['Mes', 'Total Pagos'];
    const rows = this.jsonToArray(this.pagos);

    pdf.autoTable(columns, rows,
        { margin: { top: 25 } }
    );

    pdf.save('pagosPorMes.pdf');
  }

  jsonToArray(json: any) {
    const arreglo: any[] = [];
    for ( const pago of json ) {
      const row: any[] = [];
      row.push(pago.mes);
      row.push(pago.totalPagos);

      arreglo.push(row);
    }
    return arreglo;
  }

}
