import { Component } from '@angular/core';
import { ReportesService } from '../../../services/reportes.service';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-ventas-mes-cliente',
  templateUrl: './ventas-mes-cliente.component.html',
  styleUrls: ['./ventas-mes-cliente.component.css']
})
export class VentasMesClienteComponent {

  ventasPorMesPorCliente: any[] = [];

  constructor( private reportesService: ReportesService ) {
    this.getVentasPorMesPorCliente();
  }

  getVentasPorMesPorCliente() {
    this.reportesService.getVentasPorMesPorCliente().subscribe( (data: any) => {
      this.ventasPorMesPorCliente = data;
    });
  }

  generarPDF() {
    const pdf = new jsPDF();
    pdf.text(20, 20, 'Ventas por Mes por Cliente');

    const columns = ['Mes de Venta', 'Cliente', 'Total Vendido'];
    const rows = this.jsonToArray(this.ventasPorMesPorCliente);

    pdf.autoTable(columns, rows,
        { margin: { top: 25 } }
    );

    pdf.save('ventasPorMesPorCliente.pdf');
  }

  jsonToArray(json: any) {
    const arreglo: any[] = [];
    for ( const venta of json ) {
      const row: any[] = [];
      row.push(venta.mes);
      row.push(venta.nombreCliente);
      row.push(venta.totalVendido);

      arreglo.push(row);
    }
    return arreglo;
  }

}
