import { Component, OnInit } from '@angular/core';
import { CategoriasService } from '../../services/categorias.service';
import { ArticulosService } from '../../services/articulos.service';
import { Carrito, ArticuloEnCarrito } from '../../models/Carrito';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // Almacena todas las categorias existentes en la base
  categorias: any[] = [];

  // Almacena el nombre de la categoria que sea seleciconada en el menu lateral
  nombreCategoriaSeleccionada: string;

  // Almacena el id de la categoria seleccionada en el menu lateral
  idCategoriaSeleccionada: number;

  // Almacena los articulos que se obtienen de la base de datos para ser mostrados
  articulos: any[] = [];

  // Es el articulo que se selecciona para agregar al carrito
  articuloSeleccionado: any = {};

  // Almacena la cantidad que se desea agregar al acarrito de un determinado producto
  cantidad: number;

  constructor(
    private categoriasService: CategoriasService,
    private articulosService: ArticulosService,
    private carrito: Carrito,
    private router: Router
  ) {

    this.cantidad = 1;

    this.articulosService.getAllArticulos().subscribe((data: any) => {
      this.articulos = data;
      this.idCategoriaSeleccionada = 0;
      this.nombreCategoriaSeleccionada = 'Todos los artículos';
    });
  }

  ngOnInit() {
    this.categoriasService.getCategorias().subscribe((data: any) => {
      this.categorias = data;
    });

    // this.carrito.mostrarCarrito();
  }

  mostrarTodosArticulos() {
    this.articulosService.getAllArticulos()
      .subscribe((data: any) => {
        this.articulos = data;
        this.idCategoriaSeleccionada = 0;
        this.nombreCategoriaSeleccionada = 'Todos los artículos';
      });
  }

  mostrarArticulos(idCategoria: number, nombreCategoria: string) {
    this.articulosService
      .getArticulosByIdCategoria(idCategoria)
      .subscribe((data: any[]) => {
        this.articulos = data;
        this.idCategoriaSeleccionada = idCategoria;
        this.nombreCategoriaSeleccionada = nombreCategoria;
      });
  }

  mostrarArticulosPorCategoriaYNombre(termino: string) {
    if (termino.length > 0) {
      if (this.idCategoriaSeleccionada !== 0) {
        this.articulosService.getArticulosByIdCategoriaAndTermino(this.idCategoriaSeleccionada, termino)
          .subscribe((data: any[]) => {
            this.articulos = data;
          });
      } else {
        this.articulosService.getArticulosByTermino(termino)
          .subscribe((data: any) => {
            this.articulos = data;
          });
      }
    } else {
      if (this.idCategoriaSeleccionada === 0) {
        this.mostrarTodosArticulos();
      } else {
        this.mostrarArticulos(this.idCategoriaSeleccionada, this.nombreCategoriaSeleccionada);
      }
    }
  }

  /**
   * M&eacute;todo que verifica si un articulo ya existe en el carrito de compras.
   * @param idArticulo es el id del articulo que se busca en todo el carrito para validar su existencia.
   * Si el articulo existe en el carrito se devuelve su posicion en el arreglo articulos, en caso contario
   * se devuelve un valor -1.
   */
  existeEnCarrito(idArticulo: number): number {
    let i = 0;
    for (const articulo of this.carrito.articulos) {
      if (articulo.id === idArticulo) {
        return i;
      }
      i++;
    }
    return -1;
  }

  /**
   * M&eacute;todo que agrega al carrito un articulo seleccionado, si el articulo ya habia sido agregado
   * solo se aumentara una unidad en el campo cantidad del articulo existente, en caso contario se
   * agrega el nuevo articulo con una sola unidad.
   * @param articulo es un json que viene desde la base del cual nos interesa su id, nombre y precio
   */
  addToCart(cantidad: string) {
    const posicion = this.existeEnCarrito(this.articuloSeleccionado.id);
    if (posicion === -1) {
      const nuevo: ArticuloEnCarrito = {
        id: this.articuloSeleccionado.id,
        nombre: this.articuloSeleccionado.nombre,
        precio: this.articuloSeleccionado.precio,
        cantidad: Number(cantidad)
      };

      this.carrito.articulos.push(nuevo);
    } else {
      this.carrito.articulos[posicion].cantidad += Number(cantidad);
    }
  }

  seleccionarArticulo(articulo: any) {
    this.cantidad = 1;
    this.articuloSeleccionado = articulo;
  }

}
