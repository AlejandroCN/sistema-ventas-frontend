import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PagosService {

  constructor( private http: HttpClient ) { }

  postQuery(query: string, params: any) {
    const url = `http://localhost:8080/sventas/api/pagosParciales/${query}`;

    const headersPost = new HttpHeaders().set('Content-Type', 'application/json; x-www-form-urlencoded');

    return this.http.post(url, params, {headers: headersPost});
  }

  savePago(pagoParcial: any) {
    const json = JSON.stringify(pagoParcial);
    return this.postQuery('', json).pipe(
      map(response => response as any)
    );
  }

}
