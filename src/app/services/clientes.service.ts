import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ClientesService {

    constructor(private http: HttpClient) { }

    getQuery(query: string) {
        const url = `http://localhost:8080/sventas/api/clientes/${query}`;

        return this.http.get(url);
    }

    getClienteById(id: number) {
        return this.getQuery(`cliente/${id}`).pipe(
            map(response => response as any[])
        );
    }

    getClientes() {
        return this.getQuery('').pipe(
            map(response => response as any[])
        );
    }

    getClientesByTermino(termino: string) {
        return this.getQuery(termino).pipe(
            map(response => response as any[])
        );
    }

}
