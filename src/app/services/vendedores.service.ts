import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VendedoresService {

  constructor(private http: HttpClient) {}

  getQuery(query: string) {
    const url = `http://localhost:8080/sventas/api/vendedores/${query}`;

    return this.http.get(url);
  }

  getVendedores() {
    return this.getQuery('').pipe(
      map(response => response as any[])
    );
  }

}
