import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DireccionesClienteService {

  constructor( private http: HttpClient ) { }

  getQuery(query: string) {
    const url = `http://localhost:8080/sventas/api/direccionesCliente/${query}`;

    return this.http.get(url);
  }

  postQuery(query: string, params: any) {
    const url = `http://localhost:8080/sventas/api/direccionesCliente/${query}`;

    const headersPost = new HttpHeaders().set('Content-Type', 'application/json; x-www-form-urlencoded');

    return this.http.post(url, params, {headers: headersPost});
  }

  getDireccionesCliente(idCliente: number) {
    return this.getQuery(`${idCliente}`).pipe(
      map(response => response as any[])
    );
  }

  saveDireccionCliente(direc: string, idC: number) {
    const direccionCliente = {
      id: null,
      idCliente: idC,
      direccion: direc
    };

    const json = JSON.stringify(direccionCliente);
    return this.postQuery('', json).pipe(
      map(response => response as any)
    );
  }

}
