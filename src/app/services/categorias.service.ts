import { Injectable } from '@angular/core';
// HttpClient para hacer las peticiones y los headers son para mandar el token
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';

// Indica que el componente se puede inyectar en otro componente
@Injectable({
  // Esta linea nos ahorra tener que importar el servicio en el app.module
  providedIn: 'root'
})
export class CategoriasService {

  constructor(private http: HttpClient) {}

  getQuery(query: string) {
    const url = `http://localhost:8080/sventas/api/categorias/${query}`;

    return this.http.get(url);
  }

  getCategorias() {
    return this.getQuery('').pipe(
        map(response => response as any[])
    );
  }

}
