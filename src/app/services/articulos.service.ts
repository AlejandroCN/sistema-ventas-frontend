import { Injectable } from "@angular/core";
//HttpClient para hacer las peticiones y los headers son para mandar el token
import { HttpClient } from "@angular/common/http";

import { map } from 'rxjs/operators';

//Indica que el componente se puede inyectar en otro componente
@Injectable({
  //Esta linea nos ahorra tener que importar el servicio en el app.module
  providedIn: "root"
})
export class ArticulosService {

  constructor(private http: HttpClient) {}

  getQuery(query: string) {
    const url = `http://localhost:8080/sventas/api/articulos/${query}`;

    return this.http.get(url);
  }

  getAllArticulos() {
    return this.getQuery("").pipe(
      map( response => response as any[] )
    );
  }

  getArticulosByIdCategoria(idCategoria: number=1) {
    return this.getQuery(`categoria/${idCategoria}`).pipe(
        map(response => response as any[])
    );
  }

  getArticulosByIdCategoriaAndTermino(idCategoria: number, termino: string) {
    return this.getQuery(`${idCategoria}/${termino}`).pipe(
      map(response => response as any[])
    );
  }

  getArticulosByTermino(termino: string) {
    return this.getQuery(`termino/${termino}`).pipe(
      map( response => response as any[] )
    );
  }

}
