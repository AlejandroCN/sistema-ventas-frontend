import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VentaService {

  constructor( private http: HttpClient ) { }

  postQuery(query: string, params: any) {
    const url = `http://localhost:8080/sventas/api/ventas/${query}`;

    const headersPost = new HttpHeaders().set('Content-Type', 'application/json; x-www-form-urlencoded');

    return this.http.post(url, params, {headers: headersPost});
  }

  getQuery(query: string) {
    const url = `http://localhost:8080/sventas/api/ventas/${query}`;

    return this.http.get(url);
  }

  saveVenta(ventaCompleta: any) {
    const json = JSON.stringify(ventaCompleta);
    return this.postQuery('', json).pipe(
      map(response => response as any)
    );
  }

  getVentasDeudorasByCliente(idCliente: number) {
    return this.getQuery(`${idCliente}`).pipe(
      map(response => response as any)
    );
  }

}
