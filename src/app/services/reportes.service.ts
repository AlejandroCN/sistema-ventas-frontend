import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReportesService {

  constructor( private http: HttpClient ) { }

  getQuery(query: string) {
    const url = `http://localhost:8080/sventas/api/reportes/${query}`;

    return this.http.get(url);
  }

  getVentasPorFactura() {
    return this.getQuery('/ventasFactura').pipe(
      map(response => response as any)
    );
  }

  getVentasPorArticulo() {
    return this.getQuery('/ventasArticulo').pipe(
      map(response => response as any)
    );
  }

  getVentasPorCliente() {
    return this.getQuery('/ventasCliente').pipe(
      map(response => response as any)
    );
  }

  getVentasPorMes() {
    return this.getQuery('/ventasMes').pipe(
      map(response => response as any)
    );
  }

  getVentasPorMesPorCliente() {
    return this.getQuery('/ventasMesCliente').pipe(
      map(response => response as any)
    );
  }

  getExistencias() {
    return this.getQuery('/existencias').pipe(
      map(response => response as any)
    );
  }

  getHistoricoVentas() {
    return this.getQuery('/historico').pipe(
      map(response => response as any)
    );
  }

  getPagosPorMes() {
    return this.getQuery('/pagosMes').pipe(
      map(response => response as any)
    );
  }

  getPagosPorDia() {
    return this.getQuery('/pagosDia').pipe(
      map(response => response as any)
    );
  }

}
