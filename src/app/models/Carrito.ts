import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class Carrito {

    articulos: ArticuloEnCarrito[] = [];

    constructor() {}



}

export interface ArticuloEnCarrito {
    id: number;
    nombre: string;
    precio: number;
    cantidad: number;
}
